import configparser
import functools
import os

CONFIG_LOCATIONS = ['./aptfnd.ini', os.path.expanduser('~/.aptfnd.ini')]


@functools.lru_cache()
def get_config():
    """Find aptfnd.ini and load it."""
    config = configparser.ConfigParser()
    config.read(CONFIG_LOCATIONS)
    return config
