"""Pipeline for storing apartment details in a CSV file."""

import csv
import logging

from aptfnd.config import get_config
from aptfnd.utils import safe_div, unwrap_subdict


class CSVPipeline:
    """Save apartments into a .csv file."""

    def open_spider(self, spider):
        config = get_config()
        self.dt_keys = config.options('distance-origins') + ['average']
        self.output_file = spider.name + '.csv'
        logging.debug('Saving as CSV to %s', self.output_file)
        self.file = open(self.output_file, 'w')
        self.fieldnames = [
            'title',
            'floor',
            'rooms',
            'area',
            'price',
            'price_psqm',
            'url',
            'address',
            'apt_type',
            'is_rented',
            'makler_fee',
            'has_lift',
            'has_balcony',
            'available_from',
            'heating_type',
            'hausgeld',
        ] + ['d-' + k for k in self.dt_keys] + ['t-' + k for k in self.dt_keys]
        self.csvw = csv.DictWriter(self.file, fieldnames=self.fieldnames)
        self.csvw.writeheader()

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        row = dict(item)
        unwrap_subdict(row, 'distances', 'd-')
        unwrap_subdict(row, 'times', 't-')
        self.csvw.writerow({
            k: v for k, v in row.items()
            if k in self.fieldnames
        })
        return item
