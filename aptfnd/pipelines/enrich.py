"""Enrich apartment records with additional data."""

import datetime
import logging

from aptfnd.utils import safe_int


class PPSMPipeline:
    """Add price per square meter to aparments."""

    def process_item(self, item, spider=None):
        """Calculate price per square meter (from price and area)."""
        if 'price' in item and 'area' in item:
            try:
                item['price_psqm'] = int(item['price'] / item['area'])
            except Exception as e:
                logging.debug('PPSM calculation failed', exc_info=e)
        return item


class FlagDetectionPipeline:
    """Determine if the apartment is rented, provisionfrei, etc."""

    RENTED_MARKERS = ['miete:', 'kapitalanlage', 'vermietet',
                      'frist von 10 jahren']
    PROVISION_FREI = ['provisionfrei']
    AUCTION = ['zwangsversteigerung']
    GROUND_FLOOR = ['erdgeschoss', 'erdgeschoß', 'hochparterre', 'souterrain']
    DGR = ['dachgeschossrohling', 'dachgeschoßrohling', 'ausbaukonzept',
           'dachgeschoss-rohling', 'dachgeschoß-rohling', 'dachrohling']
    HIGH_CEILING = ['altbau', 'hohe decke', 'hohen decke', 'hochdecke']

    def process_item(self, item, spider=None):
        text = (item.get('text', '') + item.get('title', '')).lower()
        for marker in self.RENTED_MARKERS:
            if marker in text:
                item['is_rented'] = 'y'
        for marker in self.PROVISION_FREI:
            if marker in text:
                item['makler_fee'] = '0'
        for marker in self.AUCTION:
            if marker in text:
                item['is_auction'] = 'y'

        if safe_int(item.get('floor', ''), 4) > 3:
            for marker in self.DGR:
                if marker in text:
                    item['is_dgr'] = 'y'

        if item.get('floor', '') == '':
            for marker in self.GROUND_FLOOR:
                if marker in text:
                    item['floor'] = 0
            if item.get('floor', '') == 0:
                item['is_ground'] = 'y'

        if item.get('is_dgr', '') != 'y':
            for marker in self.HIGH_CEILING:
                if marker in text:
                    item['high_ceiling'] = 'y'

        for flag in ['is_rented', 'is_auction']:
            if item.get(flag, '') != 'y':
                item[flag] = 'n'

        return item


class GeneralParamsPipeline:
    """Assign general parameters on the item."""

    def process_item(self, item, spider=None):
        item['spider'] = spider.name.split('-')[0]
        item['last_seen'] = datetime.datetime.now()
        item['last_retrieved'] = datetime.datetime.now()
        return item


class NumberFixPipeline:
    """Convert strings that should be numbers to numbers."""

    INTS = ['floor', 'price', 'hausgeld']
    FLOATS = ['area', 'rooms']
    REQUIRED = ['area', 'price']

    def process_item(self, item, spider=None):
        for k in self.INTS + self.FLOATS:
            if k not in item:
                continue
            value = item[k]
            if isinstance(value, int):
                continue
            if isinstance(value, float):
                if k in self.INTS:
                    item[k] = int(value)
                continue
            if value is None or value == '':
                item[k] = None
                continue
            if '.' not in value and ',' in value:
                # German decimal separator...
                value = value.replace(',', '.')
            try:
                item[k] = float(value)
                if k in self.INTS:
                        item[k] = int(item[k])
            except ValueError:
                logging.debug('Conversion of %s (%s) failed', k, item[k],
                              exc_info=True)
                if k in self.REQUIRED:
                    item[k] = -1
        return item


class StripPipeline:
    """Remove big fields to make debug output more readable."""

    TO_STRIP = ['text', 'html']

    def process_item(self, item, spider=None):
        for field in self.TO_STRIP:
            if field in item:
                del item[field]
        return item
