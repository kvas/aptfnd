"""Pipeline for calculating distances to origin using Google Maps."""

import logging
import os
import shelve

import googlemaps

from aptfnd.config import get_config


class DistancesPipeline:
    """Add distances to origins to aparments."""

    CACHE_ID_FMT = '{} -- {} by {}'

    def _open_cache(self, path):
        """Open distance cache (creating it if needed)."""
        path = os.path.expanduser(path)
        parent = os.path.dirname(path)
        os.makedirs(parent, exist_ok=True)
        self.cache = shelve.open(path)

    def open_spider(self, spider=None):
        config = get_config()
        self.origins = dict(config.items('distance-origins'))
        self.mode = config.get('distances', 'mode', fallback='bicycling')
        self.units = config.get('distances', 'units', fallback='metric')
        cache_path = config.get('distances', 'cache', fallback=None)
        if cache_path is not None:
            self._open_cache(cache_path)
        gm_key = config.get('googlemaps', 'api-key')
        qps = int(50 / len(self.origins))
        self.gm = googlemaps.Client(key=gm_key, queries_per_second=qps)

    def close_spider(self, spider=None):
        if getattr(self, 'cache', None) is not None:
            self.cache.close()

    def _query_dm(self, origin_names, destination):
        """Query DistanceMatrix for travel times."""
        logging.debug('Requesting distance matrix: %s -> %s',
                      origin_names, destination)
        dm = self.gm.distance_matrix(
            origins=origin_names,
            destinations=destination,
            mode=self.mode,
            units=self.units,
        )
        logging.debug('Got distance matrix: %s', dm)
        if dm['status'] != 'OK':
            return None
        if len(dm['rows']) != len(origin_names):
            logging.debug('Got wrong number of rows -- ignoring')
            return None
        dts = []
        for row in dm['rows']:
            item = row['elements'][0]
            if item['status'] != 'OK':
                dts.append((None, None))
            else:
                dts.append((item['distance']['value'],
                            item['duration']['value']))
        return dts

    def _load_cached_dts(self, address):
        """Load cached distance and time."""
        distances = {}
        times = {}
        for origin_id, origin_name in self.origins.items():
            cache_str = self.CACHE_ID_FMT.format(address, origin_id, self.mode)
            cached = self.cache.get(cache_str, (None, None))
            distances[origin_id], times[origin_id] = cached
        return distances, times

    def _cache_dt(self, address, origin_id, dt):
        """Store distance, time in cache."""
        cache_str = self.CACHE_ID_FMT.format(address, origin_id, self.mode)
        self.cache[cache_str] = dt

    def _get_dt(self, address):
        """Return distances and times from address to configured origins."""
        if not address:
            distances = {origin_id: None for origin_id in self.origins}
            times = {origin_id: None for origin_id in self.origins}
        else:
            distances, times = self._load_cached_dts(address)
            missing_ids = [origin_id for origin_id in self.origins
                           if times[origin_id] is None]
            if missing_ids:
                to_query = [self.origins[i] for i in missing_ids]
                dts = self._query_dm(to_query, address)
                if dts is not None:
                    for origin_id, dt in zip(missing_ids, dts):
                        self._cache_dt(address, origin_id, dt)
                        distances[origin_id], times[origin_id] = dt
        return distances, times

    def _average(self, items):
        """Calculate an average of distances or times."""
        values = [i for i in items if i is not None]
        if values:
            return sum(values) / len(values)
        else:
            return None

    def process_item(self, item, spider=None):
        address = item.get('address', None)
        if address:
            distances, times = self._get_dt(address)
            distances['average'] = self._average(distances.values())
            times['average'] = self._average(times.values())
            item['distances'] = distances
            item['times'] = times
        return item
