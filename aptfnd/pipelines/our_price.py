"""Calculate how much we would be willing to pay for apartments."""

# Parameters that go into the price calculation.
PARAMS = {
    # Base price per square meter.
    'sqm_price': 3900,
    # Area that's enough for us.
    'suf_area': 100,
    # Multiplier for additional area beyond the sufficient amount.
    'extra_area_x': 0.4,
    # Multiply area by this much if has high ceilings.
    'high_ceiling_amul': 1.07,
    # Increase area by this much if has cellar.
    'cellar_aplus': 2,
    # Multiply price by this much if located in ground floor.
    'ground_floor_mul': 0.9,
    # Multiply price by this much if has balcony.
    'balcon_mul': 1.07,
    # Multiply price by this much if rented.
    'rented_mul': 0.85,
    # Subtract this much from sqm_price for each minute over 10.
    'minutes_over_10': 20,
    # Subtract this much from sqm_price for each minute over 20.
    'minutes_over_20': 20,
    # Subtract this much from sqm_price for each minute over 30.
    'minutes_over_30': 40,
    # Default value of Hausgeld.
    'hg_base': 300,
    # Estimate hausgeld difference over this many months.
    'hg_months': 120,
}


class OurPricePipeline:
    """Add our price to apartment data."""

    def process_item(self, apt, spider=None):
        area = apt.get('area', -1)
        if area > PARAMS['suf_area']:
            extra = area - PARAMS['suf_area']
            area = PARAMS['suf_area'] + extra * PARAMS['extra_area_x']
            area = 100 + (area - 100) / 2
        if apt.get('high_ceiling', 'n') == 'y':
            area *= PARAMS['high_ceiling_amul']
        if apt.get('has_cellar', 'n') == 'y':
            area += PARAMS['cellar_aplus']

        sqm_price = PARAMS['sqm_price']
        if apt.get('floor', '') == 0:
            sqm_price *= PARAMS['ground_floor_mul']
        if apt.get('has_balcony', 'n') == 'y':
            sqm_price *= PARAMS['balcon_mul']
        if apt.get('is_rented', 'n') == 'y':
            sqm_price *= PARAMS['rented_mul']

        seconds = apt.get('t-work', 1800)
        if seconds is None:
            seconds = 1800
        minutes = seconds / 60
        sqm_price -= max(minutes - 10, 0) * PARAMS['minutes_over_10']
        sqm_price -= max(minutes - 20, 0) * PARAMS['minutes_over_20']
        sqm_price -= max(minutes - 30, 0) * PARAMS['minutes_over_30']

        price = area * sqm_price

        hg = apt.get('hausgeld', None) or PARAMS['hg_base']
        hg_diff = hg - PARAMS['hg_base']
        price -= hg_diff * PARAMS['hg_months']

        apt['our_price'] = price
        return apt
