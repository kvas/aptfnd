"""Pipeline for storing apartment details to MongoDB database."""

import datetime

from aptfnd.backend import get_storage
from aptfnd.utils import unwrap_subdict


class MongoPipeline:
    """Save apartments into MongoDB database."""

    def open_spider(self, spider):
        self.col = get_storage().apartments

    def process_item(self, item, spider):
        """Save item to MondgoDB."""
        if item.get('spider_id', '') == '' or item.get('spider', '') == '':
            # Need spider_id and spider to ensure uniqueness.
            return item

        updates = {k: v for k, v in item.items() if v is not None and v != ''}
        unwrap_subdict(updates, 'distances', 'd-')
        unwrap_subdict(updates, 'times', 't-')
        self.col.find_and_modify(
            {'spider': item['spider'], 'spider_id': item['spider_id']},
            {
                '$set': updates,
                '$setOnInsert': {'first_seen': datetime.datetime.now()},
            },
            upsert=True,
        )
        return item
