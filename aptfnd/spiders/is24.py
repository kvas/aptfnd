import json
import re

from aptfnd.items import Apartment
from aptfnd.spiders.base import AptSpider
from aptfnd.utils import xpath2str

EXPOSE_PATTERN = re.compile(r'/expose/\d+$')
SEARCH_PATTERN = re.compile(r'/Suche/S-T/P-\d+/')

# TITLE_XPATH = "//meta[@name='description']/@content"
TITLE_XPATH = "//h1[@id='expose-title']//text()"
ADDRESS_XPATH = "//span[@data-qa='is24-expose-address']//text()"
MAKLER_FEE_XPATH = "//dd[contains(@class, 'is24qa-provision ')]//text()"
TEXT_XPATH = "//div[contains(@class, 'is24-text')]//text()"
HTML_XPATH = "//div[@id='is24-content']/div[2]"
RENT_XPATH = "//dd[contains(@class, 'is24qa-mieteinnahmen')]//text()"
HG_XPATH = "//dd[contains(@class, 'is24qa-hausgeld ')]//text()"
BF_XPATH = "//dd[contains(@class, 'is24qa-bezugsfrei-ab ')]//text()"
TYP_XPATH = "//dd[contains(@class, 'is24qa-typ ')]//text()"

RENTED_MARKERS = ['miete:', 'kapitalanlage', 'vermietet']

PARAMS = {
    'spider_id': 'obj_scoutId',
    'is_rented': 'obj_rented',
    'apt_type': 'obj_immotype',
    'heating_type': 'obj_heatingType',
    'area': 'obj_livingSpace',
    'price': 'obj_purchasePrice',
    'has_lift': 'obj_lift',
    'has_balcony': 'obj_balcony',
    'has_cellar': 'obj_cellar',
    'has_kitchen': 'obj_hasKitchen',
    'condition': 'obj_condition',
    'rooms': 'obj_noRooms',
    'floor': 'obj_floor',
    'makler_fee': 'obj_courtage',
}


class ImmobilienScout24Spider(AptSpider):
    """Spider for ImmobilienScout24.

    This spider returns full Apartment records. It uses `get_urls_to_skip`
    to avoid refetching the apartments were recently downloaded.
    """

    name = 'is24'
    allowed_domains = ['www.immobilienscout24.de']

    def is_expose(self, response):
        return EXPOSE_PATTERN.search(response.url)

    def extract_links(self, response):
        for url in response.xpath('//a/@href').extract():
            main = url.split('?')[0]
            if EXPOSE_PATTERN.search(main) or SEARCH_PATTERN.search(main):
                yield response.follow(url)

    def _extract_params(self, item, params):
        for param, info in PARAMS.items():
            item[param] = params.get(info, None)

    def parse_expose(self, response):
        item = Apartment(url=response.url)

        item['title'] = xpath2str(response.xpath(TITLE_XPATH))
        address = xpath2str(response.xpath(ADDRESS_XPATH))
        item['address'] = address.split('Die vollständige Adresse')[0]
        item['available_from'] = xpath2str(response.xpath(BF_XPATH))

        hg_match = re.search('[\d\,]+', xpath2str(response.xpath(HG_XPATH)))
        if hg_match:
            item['hausgeld'] = hg_match.group(0).replace(',', '.')

        item['text'] = xpath2str(response.xpath(TEXT_XPATH))
        item['html'] = xpath2str(response.xpath(HTML_XPATH))

        lines = response.text.splitlines()
        for line in lines:
            line = line.strip()
            if line.startswith('var keyValues = {'):
                item['html'] += '\n' + line
                line = line[16:].rstrip(';')
                params = json.loads(line)
                self._extract_params(item, params)

        # If rent amount is specified, it's rented.
        if xpath2str(response.xpath(RENT_XPATH)):
            item['is_rented'] = 'y'

        makler_fee = xpath2str(response.xpath(MAKLER_FEE_XPATH))
        if makler_fee:
            item['makler_fee'] = makler_fee

        apt_type = xpath2str(response.xpath(TYP_XPATH))
        if apt_type.lower().startswith('erdge'):
            item['floor'] = '0'

        yield item
