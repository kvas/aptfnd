import json

from aptfnd.items import Apartment
from aptfnd.spiders.base import AptSpider
from aptfnd.utils import xpath2str, xpath2str_c, compress_space

ANZ_PATTERN = '/s-anzeige/'

ANZ_LINK_XPATH = ("//div[@class='aditem-main']/h2[@class='text-module-begin']"
                  "/a/@href")
NEXT_LINK_XPATH = "//a[@class='pagination-next']/@href"

TITLE_XPATH = "//h1[@id='viewad-title']//text()"
DETAILS_XPATH = "//section[@itemprop='offerDetails']"
PRICE_XPATH = ".//meta[@itemprop='price']/@content"
ATTR_KEYS_XPATH = "./dl/dt[@class='attributelist--key']"
ATTR_VALUES_XPATH = "./dl/dd[@class='attributelist--value']"
TEXT_XPATH = "//p[@id='viewad-description-text']/text()"

PARAMS = {
    'spider_id': 'adid',
    'price': 'ExactPreis',
    'makler_fee': 'Provision',
    'rooms': 'Zimmer',
    'hausgeld': 'Hausgeld__€_',
    'heating_type': 'Heizungsart',
    'apt_type': 'Wohnungstyp',
    'floor': 'Etage',
}

ATTRIBUTES = {
    'price': 'price',
    'address': 'Ort',
    'hausgeld': 'Hausgeld (€)',
    'heating_type': 'Heizungsart',
    'makler_fee': 'Provision',
    'rooms': 'Zimmer',
    'apt_type': 'Wohnungstyp',
    'area': 'Wohnfläche (m²)',
    'floor': 'Etage',
}


class EbayKASpider(AptSpider):
    """Spider for Ebay Kleinanzeigen.

    This spider returns full Apartment records. It uses `get_urls_to_skip`
    to avoid refetching the apartments were recently downloaded.
    """

    name = 'ebayka'
    allowed_domains = ['www.ebay-kleinanzeigen.de']

    def is_expose(self, response):
        return ANZ_PATTERN in response.url

    def extract_links(self, response):
        for url in (response.xpath(ANZ_LINK_XPATH).extract() +
                    response.xpath(NEXT_LINK_XPATH).extract()):
            yield response.follow(url)

    def _get_json_params(self, response):
        json_lines = []
        in_json = False
        for line in response.text.splitlines():
            line = line.strip()
            if in_json:
                if line == '});':
                    json_lines.append('}')
                    return json.loads('\n'.join(json_lines))
                json_lines.append(line)
            if line.startswith('Belen.Advertiser.Headerbidding.requestBids('):
                json_lines.append('{')
                in_json = True
        return {}

    def _extract_params(self, response, item):
        params = self._get_json_params(response).get('extParams', {})
        for key, name in PARAMS.items():
            if not item.get(key, None):
                item[key] = params.get(name, None)

        if params.get('Balkon', '') == 'true':
            item['has_balcony'] = 'y'
        if params.get('Aktuell_vermietet', '') == 'true':
            item['is_rented'] = 'y'

    def _get_attributes(self, response):
        details_elt = response.xpath(DETAILS_XPATH)
        attributes = {
            'price': xpath2str(details_elt.xpath(PRICE_XPATH))
        }
        keys = [
            xpath2str(k.xpath('.//text()'))
            for k in details_elt.xpath(ATTR_KEYS_XPATH)
        ]
        values = [
            xpath2str(v.xpath('.//text()'))
            for v in details_elt.xpath(ATTR_VALUES_XPATH)
        ]
        for key, value in zip(keys, values):
            key = key.strip().strip(':')
            attributes[key] = compress_space(value.strip())
        return attributes

    def _extract_attributes(self, response, item):
        attributes = self._get_attributes(response)
        for key, name in ATTRIBUTES.items():
            if not item.get(key, None):
                item[key] = attributes.get(name, None)

        others = attributes.get('Ausstattung', '')
        item['text'] = others

        if 'Balkon' in others:
            item['has_balcony'] = 'y'

        if 'Verfügbar ab Jahr' in others:
            item['available_from'] = others['Verfügbar ab Jahr']
            if 'Verfügbar ab Monat' in others:
                item['available_from'] += '/' + others['Verfügbar ab Monat']

    def parse_expose(self, response):
        item = Apartment(url=response.url)

        item['title'] = xpath2str(response.xpath(TITLE_XPATH))
        self._extract_attributes(response, item)
        self._extract_params(response, item)

        item['text'] += '\n' + xpath2str_c(response.xpath(TEXT_XPATH))
        item['html'] = xpath2str_c(response.xpath('.'))

        if item['area']:
            item['area'] = item['area'].replace(',', '.')

        if item['apt_type'] == 'Erdgeschosswohnung':
            item['floor'] = '0'

        yield item
