"""Base class for apartment website scrapers."""

import logging

import scrapy

from aptfnd.config import get_config
from aptfnd.backend import get_urls_to_skip


class AptSpider(scrapy.Spider):
    """Base functionality for apartment website scrapers."""

    detailed = False

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self.seen_urls = set()
        self.to_skip = set(get_urls_to_skip(self.name))
        query = get_config().get(self.name, 'query')
        self.start_urls = filter(None, query.split('\n'))

    def parse(self, response):
        """Redirect to customized expose parsing and link extraction."""
        if self.is_expose(response):
            yield from self.parse_expose(response)
        else:
            for req in self.extract_links(response):
                url = req.url
                if url in self.seen_urls:
                    logging.debug('Skipped duplicate: %s', url)
                    continue
                if url in self.to_skip:
                    logging.debug('Recently downloaded: %s', url)
                    continue
                self.seen_urls.add(url)
                yield req
