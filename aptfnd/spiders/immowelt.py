import json

from aptfnd.items import Apartment
from aptfnd.spiders.base import AptSpider
from aptfnd.utils import xpath2str, xpath2str_c, get_number_g

EXPOSE_PATTERN = '/expose/'
LIST_PATTERN = '/liste/'

APT_LINK_XPATH = "//div[contains(@class, 'iw_list_content')]//a/@href"
NEXT_XPATH = "//a[@id='nlbPlus']/@href"
APT_XPATH = ("//div[contains(@class, 'iw_list_content')]//"
             "div[contains(@class, 'listitem_wrap')]")

JSON_XPATH = '//body/script[1]/text()'
EXPOSE_XPATH = "//div[@id='expose']"
TITLE_XPATH = ".//h1[1]//text()"
SECTION_CONTENT_XPATH = "..//div[contains(@class, 'section_content')]//text()"
ADDRESS_CSS = 'div.quickfacts div.location span.no_s'

PARAMS = {
    'price': 'object_price',
    'rooms': 'object_rooms',
    'area': 'object_area',
    'options': 'object_features',
}


class ImmoweltSpider(AptSpider):
    """Spider for immowelt.de."""

    name = 'immowelt'
    allowed_domains = ['www.immowelt.de']

    def is_expose(self, response):
        main = response.url.split('?')[0]
        return EXPOSE_PATTERN in main

    def extract_links(self, response):
        for url in (response.xpath(APT_LINK_XPATH).extract() +
                    response.xpath(NEXT_XPATH).extract()):
            if EXPOSE_PATTERN in url or LIST_PATTERN in url:
                yield response.follow(url)

    def _get_params(self, response, item):
        script1 = xpath2str(response.xpath(JSON_XPATH))
        item['html'] += '\n' + script1
        if '{' in script1 and '};' in script1:
            json_str = '{' + script1.split('{', 1)[1].split('};', 1)[0] + '}'
            return json.loads(json_str)
        else:
            return {}

    def _extract_params(self, response, item):
        params = self._get_params(response, item)
        for k, v in PARAMS.items():
            item[k] = params.get(v, '')

        if ('vermietet' not in item['options'] and
                item.get('is_rented', '') != 'y'):
            item['is_rented'] = 'n'
        if 'provisionfrei' in item['options']:
            item['makler_fee'] = '0'
        if 'Balkon' in item['options']:
            item['has_balcony'] = 'y'
        if 'Kelleranteil' in item['options']:
            item['has_cellar'] = 'y'
        if 'Einbauküche' in item['options']:
            item['has_kitchen'] = 'y'

        item['text'] += '\n' + ', '.join(item['options'])

    def parse_expose(self, response):
        item = Apartment(url=response.url)
        item['spider_id'] = item['url'].split('?')[0].split('/')[-1]

        expose = response.xpath(EXPOSE_XPATH)
        item['html'] = xpath2str(expose)
        item['text'] = ''
        item['title'] = xpath2str_c(expose.xpath(TITLE_XPATH))
        item['address'] = xpath2str_c(expose.css(ADDRESS_CSS)
                                      .xpath('.//text()'))

        for row in expose.css('div.datarow'):
            label = xpath2str_c(row.css('div.datalabel').xpath('.//text()'))
            value = xpath2str_c(row.css('div.datacontent').xpath('.//text()'))
            if label == 'Hausgeld':
                item['hausgeld'] = get_number_g(value)

        for sec in expose.css('div.section_label'):
            label = xpath2str_c(sec.xpath('.//text()'))
            value = xpath2str_c(sec.xpath(SECTION_CONTENT_XPATH))
            if label == 'Käuferprovision':
                item['makler_fee'] = value.split('Zuschüsse')[0]
            if label == 'Die Wohnung':
                if 'Geschoss' in value:
                    item['floor'] = (value.split('Geschoss')[0]
                                     .split('Etagenwohnung')[-1])
                if 'Erdgeschoss' in value:
                    item['floor'] = 0
                if 'vermietet' in value:
                    item['is_rented'] = 'y'
                if 'frei ab' in value:
                    t = value.split('frei ab')[1].split()
                    item['available_from'] = ' '.join(t[:2])
                elif 'frei' in value:
                    item['is_rented'] = 'n'
            if label == 'Energie / Versorgung':
                item['heating_type'] = value.split('Stromverbrauch ermitt')[0]
            if label in ['Ausstattung', 'Objekt', 'Sonstiges', 'Stichworte']:
                item['text'] += '\n' + value

        self._extract_params(response, item)

        yield item
