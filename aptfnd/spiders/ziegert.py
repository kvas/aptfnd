import re

from aptfnd.spiders.base import AptSpider
from aptfnd.items import Apartment
from aptfnd.utils import xpath2str_c


PROJECT_PATTERN = 'project/?identifier='


def get_cell(row, cell_id):
    """Extract a cell from a table of apartments."""
    xpath_expr = "./td[@data-label='{}']/span/p/text()".format(cell_id)
    return xpath2str_c(row.xpath(xpath_expr))


class ZiegertSpider(AptSpider):
    """Spider for Ziegert Immobilien website.

    This spider finds project pages using search and then extracts individual
    apartments from them.
    """

    name = 'ziegert'
    allowed_domains = ['ziegert-immobilien.de']

    def is_expose(self, response):
        return PROJECT_PATTERN in response.url

    def extract_links(self, response):
        for url in response.xpath('//a/@href').extract():
            if PROJECT_PATTERN in url:
                yield response.follow(url)

    def parse_expose(self, response):
        title = xpath2str_c(response.xpath('//h1/text()'))
        address = ''

        lines = response.text.splitlines()
        for line in lines:
            if "input[name*='apartmentstreet']" in line:
                m = re.search(r'\.val\("([^"]*)"\)', line, flags=re.M)
                if m:
                    address = m.group(1)
                else:
                    address = line

        equipment = response.css('div.equipment')
        equipment_text = xpath2str_c(equipment.xpath('.//text()'))
        equipment_html = xpath2str_c(equipment)

        for unit_tr in response.xpath("//table[@id='unitslist']/tbody/tr"):
            area = get_cell(unit_tr, 'Wohnfläche')
            if area.endswith(' m'):
                area = area[:-2]

            price = get_cell(unit_tr, 'Preis')
            price = price.split(' ')[0].replace('.', '')

            etage = get_cell(unit_tr, 'Etage')
            if 'EG' in etage:
                etage = 0
            elif 'DG' in etage:
                etage = -1
            elif 'OG' in etage:
                t = re.search('\d+', etage)
                if t:
                    etage = t.group(0)

            yield Apartment(
                url=response.url,
                title=title,
                address=address,
                spider_id=get_cell(unit_tr, 'ID'),
                is_rented=(
                    'y' if 'vermiet' in get_cell(unit_tr, 'Einheit').lower()
                    else 'n'
                ),
                apt_type=get_cell(unit_tr, 'Einheit'),
                floor=etage,
                rooms=get_cell(unit_tr, 'Zimmer'),
                area=area,
                price=price,
                text=(xpath2str_c(unit_tr.xpath('.//text()')) +
                      '\n' + equipment_text),
                html=xpath2str_c(unit_tr) + '\n' + equipment_html,
            )
