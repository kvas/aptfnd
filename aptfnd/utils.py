"""Common utility functions."""

import re


def compress_space(txt):
    """Convert ranges of whitespace to single spaces."""
    return re.sub(r'\s+', ' ', txt)


def xpath2str(sel):
    """Return the results of XPATH query as a string."""
    return ' '.join(sel.extract()).strip()


def xpath2str_c(sel):
    """Shorthand for compress_space(xpath2str(sel))."""
    return compress_space(xpath2str(sel))


def extract_text(sel):
    """Extract all text from XPATH query result."""
    return xpath2str(sel.xpath('.//text()'))


def extract_text_c(sel):
    """Shorthand for compress_space(extract_text(sel))."""
    return compress_space(extract_text(sel))


def safe_div(v, d):
    if v is None:
        return ''
    return '{:.1f}'.format(v / d)


def safe_int(s, default):
    if isinstance(s, str):
        t = re.search(r'[\+\-\d]+', s)
        if t:
            s = t.group(0)
    try:
        return int(s)
    except (ValueError, TypeError):
        return default


def get_number_g(value):
    """Extract number in German decimal format (using comma)."""
    t = re.search(r'[\d\,]+', value)
    if t:
        return t.group(0)


def unwrap_subdict(item, subdict_name, prefix):
    if subdict_name in item:
        item.update({prefix + k: v for k, v in item[subdict_name].items()})
        del item[subdict_name]
