# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Apartment(scrapy.Item):
    """Description of an apartment."""
    # Which spider downloaded this.
    spider = scrapy.Field()
    # Unique ID assigned by the spider (usually id at the website).
    spider_id = scrapy.Field()
    # URL of the most detailed page that shows this apartment.
    url = scrapy.Field()
    # When have we seen it for the first time.
    first_seen = scrapy.Field()
    # When have we seen it one any page.
    last_seen = scrapy.Field()
    # When have we retrieved the page given by `url` parameter.
    last_retrieved = scrapy.Field()
    # Human-readable title.
    title = scrapy.Field()
    # Street address (as detailed as we know).
    address = scrapy.Field()
    # Is the apartment rented?
    is_rented = scrapy.Field()
    # In which floor is the apartment (0 = ground floor).
    floor = scrapy.Field()
    # How many rooms does it have.
    rooms = scrapy.Field()
    # Area of the apartment in square meters.
    area = scrapy.Field()
    # Price of the whole apartment in Euro.
    price = scrapy.Field()
    # Price of the apartment for us.
    our_price = scrapy.Field()
    # Price per square meter in Euro.
    price_psqm = scrapy.Field()
    # List of options like "balcony", "guest wc", etc.
    options = scrapy.Field()
    # Type, like "dachgeschosswohnung". Usually provided by the website.
    apt_type = scrapy.Field()
    # Heating type, provided by website.
    heating_type = scrapy.Field()
    # Distances to configured origin points in meters.
    distances = scrapy.Field()
    # Biking times to configured origin points in seconds.
    times = scrapy.Field()
    # Condition of the apartment.
    condition = scrapy.Field()
    # Some feature flags.
    has_lift = scrapy.Field()
    has_balcony = scrapy.Field()
    has_kitchen = scrapy.Field()
    has_cellar = scrapy.Field()
    high_ceiling = scrapy.Field()
    is_ground = scrapy.Field()
    is_dgr = scrapy.Field()
    is_auction = scrapy.Field()
    # The date from which the apartment is available.
    available_from = scrapy.Field()
    # Monthly payments.
    hausgeld = scrapy.Field()
    # Makler fee in percents.
    makler_fee = scrapy.Field()
    # All text related to the aparment combined.
    text = scrapy.Field()
    # The HTML code of page at `url` or its relevant part.
    html = scrapy.Field()
