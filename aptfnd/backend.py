from datetime import datetime, timedelta
import functools
from urllib.parse import urlparse

import pymongo

from aptfnd.config import get_config

# Default database name.
DEFAULT_DB = 'aptfnd'


@functools.lru_cache()
def get_storage():
    """Return MongoDB storage backend."""
    config = get_config()
    connstr = config.get('backend', 'mongodb')
    dbname = urlparse(connstr).path.strip('/') or DEFAULT_DB
    return pymongo.MongoClient(connstr)[dbname]


@functools.lru_cache()
def _age_cutoff():
    """After which date do we refresh apartments."""
    config = get_config()
    days = config.getint('general', 'refresh-after-days', fallback=5)
    return datetime.now() - timedelta(days=days)


def get_apartments(query, projection=None):
    """Query and return apartments."""
    return get_storage().apartments.find(query, projection=projection)


def get_urls_to_skip(spider):
    """Get aparment URLs to skip (recently retrieved)."""
    records = get_apartments({
        'spider': spider,
        'last_retrieved': {'$gt': _age_cutoff()},
    }, projection=['url'])
    return [r['url'] for r in records]
