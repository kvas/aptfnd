"""Telegram bot that informs people about new apartments."""

import datetime
import logging
import os
import pprint
import subprocess
import urllib.parse as up

import telegram.ext as te

import aptfnd.config as conf
import aptfnd.backend as bk


class AptFndUpdater(te.Updater):
    """Custom updater."""

    TEMPLATE = (
        "[{0[title]}]({0[url]})\n"
        "{0[area]} m2 for {0[price_k]}k (our price: {0[our_price_k]}k)\n"
        "floor: {0[floor]}, time to work: {0[m-work]} min\n"
        "[{0[address]}]({0[addr_url]})"
    )

    def __init__(self, update_interval=300, crawl_interval=3600,
                 expiration_days=1):
        config = conf.get_config()
        key = config.get('telegram', 'key')
        super().__init__(key)

        chats = [
            l.strip()
            for l in config.get('telegram', 'chats', fallback='').split('\n')
        ]
        self.chat_ids = set(map(int, filter(None, chats)))
        self.control_chat_id = int(config.get('telegram', 'control_chat',
                                   fallback='0'))
        self.expiration_days = expiration_days
        self.info = {
            'chats': ', '.join(str(c) for c in self.chat_ids),
            'control_chat': self.control_chat_id,
            'crawl_interval': crawl_interval,
            'update_interval': update_interval,
            'expiration_days': expiration_days,
        }

        dp = self.dispatcher
        dp.add_handler(te.CommandHandler('crawl', self.crawl))
        dp.add_handler(te.CommandHandler('help', self.help))
        dp.add_handler(te.CommandHandler('info', self.show_info))
        dp.add_handler(te.CommandHandler('start', self.start))
        dp.add_handler(te.CommandHandler('updates', self.updates))
        dp.add_error_handler(self.error)

        self.job_queue.run_repeating(self.post_updates,
                                     interval=update_interval, first=0)
        self.job_queue.run_repeating(self.start_crawl, interval=crawl_interval)

    def start(self, bot, update):
        """Send a message when the command /start is issued."""
        message = update.message
        chat_id = message.chat.id
        if chat_id in self.chat_ids:
            message.reply_text("I'm already posting to this chat.")
        else:
            message.reply_text('Add {} to chats config to post to this chat.'
                               .format(chat_id))

    def _get_updates(self, chat_ids=None):
        """Find new updates for selected chats."""
        delta = datetime.timedelta(days=self.expiration_days)
        start = datetime.datetime.now() - delta
        query = {
            'first_seen': {'$gt': start},
            'price': {'$lt': 410000},
            'area': {'$gt': 75},
            't-work': {'$lt': 1700},
            'is_rented': 'n',
            'is_auction': 'n',
            'is_dgr': {'$nin': ['y']},
            'available_from': {'$nin': ['2020', 'Ende 2019']},
        }
        if chat_ids is not None:
            query['notified_chat_ids'] = {'$not': {'$all': list(chat_ids)}}
        return bk.get_apartments(query)

    def _format_apartment(self, apt):
        """Format apartment into a Markdown message for posting."""
        apt = dict(apt)
        apt['m-work'] = int(apt.get('t-work', 0) / 60)
        apt['price_k'] = int(apt.get('price', 0) / 1000)
        apt['our_price_k'] = int(apt.get('our_price', 0) / 1000)
        apt['addr_url'] = ('https://maps.google.com/maps?' +
                           up.urlencode({'q': apt['address']}))
        for key in ['floor']:
            apt[key] = apt.get(key, '?')
        return self.TEMPLATE.format(apt)

    def _apartment_notify(self, apt, chat_id=None):
        """Post apartment to chats that haven't seen it yet."""
        notified_chat_ids = set(apt.get('notified_chat_ids', []))
        if chat_id is None:
            chats_to_notify = self.chat_ids - notified_chat_ids
        else:
            chats_to_notify = {chat_id}
        message = self._format_apartment(apt)
        if chats_to_notify:
            for chat_id in chats_to_notify:
                logging.debug('Sending apt %s to chat %d', apt['_id'], chat_id)
                self.bot.send_message(chat_id, message, parse_mode='Markdown')
                notified_chat_ids.add(chat_id)
            logging.debug('Saving apt %s', apt['_id'])
            bk.get_storage().apartments.update_one(
                {'_id': apt['_id']},
                {'$set': {'notified_chat_ids': list(notified_chat_ids)}},
            )

    def post_updates(self, bot, queue):
        """Post new updates into all chats."""
        logging.debug('Posting updates to chats: %s', self.chat_ids)
        for apt in self._get_updates(self.chat_ids):
            self._apartment_notify(apt)
        self.info['last_updates'] = str(datetime.datetime.now())

    def updates(self, bot, update):
        """Show matching apartments from last day."""
        for apt in self._get_updates():
            self._apartment_notify(apt, chat_id=update.message.chat.id)

    def _check_control(self, update):
        """Check if the chat of this update is the control chat."""
        message = update.message
        if message.chat.id != self.control_chat_id:
            message.reply_text('This should be posted to control chat.')
            return False
        return True

    def _check_crawl(self):
        """Check if the crawler is active and save return code and output.

        Returns True if the crawler is running, False otherwise.
        """
        if hasattr(self, 'crawl_process'):
            rc = self.crawl_process.poll()
            if rc is None:
                self.info['crawler_rc'] = None
                self.info['crawler_running'] = True
            else:
                self.info['crawler_rc'] = rc
                self.info['crawler_running'] = False
        else:
            self.info['crawler_rc'] = None
            self.info['crawler_running'] = False
        return self.info['crawler_running']

    def _start_crawl(self):
        """Start the crawler."""
        if self._check_crawl():
            return False
        path = os.path.join(os.path.dirname(__file__), 'crawl-all.sh')
        self.crawl_process = subprocess.Popen(path)
        self.info['crawler_started'] = str(datetime.datetime.now())
        return True

    def crawl(self, bot, update):
        """Run the crawler."""
        if not self._check_control(update):
            return
        if self._start_crawl():
            update.message.reply_text('Crawl started')
        else:
            update.message.reply_text('Crawl already in progress')

    def start_crawl(self, bot, queue):
        """Run a regular crawl."""
        self._start_crawl()

    def show_info(self, bot, update):
        """Post info about the state of the bot."""
        if not self._check_control(update):
            return
        self._check_crawl()
        update.message.reply_text(pprint.pformat(self.info))

    def help(self, bot, update):
        """Send a message when the command /help is issued."""
        update.message.reply_text('I have no idea how to use me :/')

    def error(self, bot, update, error):
        """Log Errors caused by Updates."""
        logging.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Run the bot."""
    updater = AptFndUpdater(30, 1800)
    updater.start_polling(poll_interval=3)
    updater.idle()


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    )
    main()
