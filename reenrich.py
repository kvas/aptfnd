#!/usr/bin/env python
"""
Derive additional parameters based on the ones we already have.

This script looks at the text, html and possibly other, more specific,
parameters of the apartments records in the database and adds or corrects
more specific flags that can be used for searches.
"""

import argparse
import pprint
import sys

from aptfnd.backend import get_apartments, get_storage
from aptfnd.pipelines.enrich import FlagDetectionPipeline, NumberFixPipeline
from aptfnd.pipelines.our_price import OurPricePipeline

HIDE_KEYS = ['text', 'html']


def _diff(old, new):
    diff = {}
    for k in sorted(set(new) | set(old)):
        was = old.get(k, None)
        now = new.get(k, None)
        if was != now:
            diff[k] = (was, now)
    return diff


def main(args):
    apartments = get_apartments({})
    pipelines = [
        NumberFixPipeline(),
        FlagDetectionPipeline(),
        OurPricePipeline(),
    ]
    change_count = 0
    write_count = 0
    for apt in apartments:
        try:
            modified = dict(apt)
            for p in pipelines:
                modified = p.process_item(apt)
        except Exception as exc:
            to_show = dict(modified)
            for k in ['html', 'text']:
                if k in to_show:
                    del to_show[k]
            sys.stderr.write('Error when enriching ({})\n'.format(exc))
            sys.stderr.write(pprint.pformat(to_show) + '\n')
        if modified != apt:
            change_count += 1
            diff = _diff(apt, modified)
            if diff and args.write_changes:
                res = get_storage().apartments.update_one(
                    {'_id': apt['_id']},
                    {'$set': {k: v[1] for k, v in diff.items()}},
                )
                assert res.modified_count == 1
                write_count += 1
            if args.verbose:
                print('-->', apt['url'], '(' + apt['title'] + ')')
                print('| TEXT:', apt.get('text', None))
                for k, (was, now) in diff.items():
                    print('!', k, was, '->', now)
                print()
            else:
                sys.stdout.write('.')
                sys.stdout.flush()
    print('changed:', change_count, 'written:', write_count)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-v', '--verbose', action='count',
                        help='Tell me more')
    parser.add_argument('-w', '--write-changes', action='store_true',
                        help='Write changes to the database')
    args = parser.parse_args()
    main(args)
