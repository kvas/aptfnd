#!/bin/bash

set -e

# echo Started fake crawling
# sleep 30
# echo Ended fake crawling
# exit 0

. venv/bin/activate

for i in is24 immowelt ebayka ziegert
do
    echo "Starting $i crawler"
    scrapy crawl $i >>crawl.log 2>&1
    echo "Done with $i crawler"
done
